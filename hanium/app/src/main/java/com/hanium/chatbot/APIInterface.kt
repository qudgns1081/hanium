package com.hanium.chatbot

import retrofit2.Call
import retrofit2.http.*

data class UserInfo(var results:String?,var msg:String?)
data class ContentData(var id:String?,var content:String?)
data class AuthData( var code:String?)
data class ChatList(var Items:ArrayList<ChatData>)
data class ChatData(var content:String?, var no: String?, var date: Long?)

interface Login {
    @FormUrlEncoded
    @POST("/login")
    fun postRequest(@Field("id")id: String,
                    @Field("pw")pw: String): Call<UserInfo>
}
interface Signup {
    @FormUrlEncoded
    @POST("/signup")
    fun postRequest(@Field("id")id: String,
                    @Field("pw")pw: String): Call<UserInfo>
}
interface IdCheck {
    @FormUrlEncoded
    @POST("/idcheck")
    fun postRequest(@Field("id")id: String): Call<AuthData>
}
interface ContentGet {
    @GET("/content_get")
    fun postRequest(): Call<ContentData>
}
interface ContentInput {
    @FormUrlEncoded
    @POST("/content_input")
    fun postRequest(@Field("id")id: String,
                    @Field("content")content:String): Call<ContentData>
}
interface AuthMail {
    @FormUrlEncoded
    @POST("/authmail")
    fun postRequest(@Field("email")email: String): Call<AuthData>
}
interface GetContent {
    @FormUrlEncoded
    @POST("/getContent")
    fun postRequest(@Field("id") id: String,
                    @Field("start") start: Long?
    ): Call<ArrayList<ChatData>>
}