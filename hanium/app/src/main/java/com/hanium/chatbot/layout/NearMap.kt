package com.hanium.chatbot.layout

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.hanium.chatbot.ChatData
import com.hanium.chatbot.R
import com.hanium.chatbot.adapter.ChatAdapter
import com.hanium.chatbot.adapter.LocationAdapter
import com.hanium.chatbot.kakaoMap.*
import io.nlopez.smartlocation.OnLocationUpdatedListener
import io.nlopez.smartlocation.SmartLocation
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider
import net.daum.mf.map.api.MapPOIItem
import net.daum.mf.map.api.MapPoint
import net.daum.mf.map.api.MapView
import retrofit2.Call
import retrofit2.Response

class NearMap:AppCompatActivity(), OnLocationUpdatedListener {
    private lateinit var mMap: GoogleMap
    private lateinit var mapView : MapView
    private val kakaoApi = KakaoApiRetrofitClient.apiService
    private lateinit var mapPoint : MapPoint
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nearmap)
        mapView = MapView(this)
        val locationManager = this.getSystemService(LOCATION_SERVICE) as LocationManager
        val mapViewContainer: ConstraintLayout = findViewById(R.id.map_view)
        val bottomSheet:LinearLayout = findViewById(R.id.bottom_sheet)
        val sheetBehavior= BottomSheetBehavior.from(bottomSheet)
        val upDownB : ImageButton = findViewById(R.id.upDownB)
        var check = 2

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : GPS 가 켜져있지 않으면 system 에서 활성화 페이지 이동
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
        Handler(Looper.getMainLooper()).postDelayed({
            mapViewContainer.addView(mapView)
            getGps()
        },500)


        // 작성일자 22.05.12
        // 작성자 : 이병훈
        // 기능 : BottomSheet 클래스 정의
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //슬라이드 될 때
            }
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when(newState) {
                    BottomSheetBehavior.STATE_COLLAPSED-> {
                        upDownB.setImageResource(R.drawable.up)
                        check = 1
                    }
                    BottomSheetBehavior.STATE_DRAGGING-> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED-> {
                        upDownB.setImageResource(R.drawable.down)
                        check =2
                    }
                    BottomSheetBehavior.STATE_HIDDEN-> {
                    }
                    BottomSheetBehavior.STATE_SETTLING-> {
                    }
                }
            }
        })
        sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        upDownB.setOnClickListener {
            if(check == 1) {
                sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }else if(check == 2) {
                sheetBehavior.state=BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    // 작성일자 22.05.11
    // 작성자 : 이병훈
    // 기능 : GPS 로 현재 위치 좌표를 가져옴
    private fun getGps(){
        val provider = LocationGooglePlayServicesProvider()
        provider.setCheckLocationSettings(true)
        val smartLocation = SmartLocation.Builder(this).logging(true).build()
        smartLocation.location(provider).start(this)
    }

    // 작성일자 22.05.11
    // 작성자 : 이병훈
    // 기능 : GPS 좌표가 변경 될 때 마커와 키워드 검색결과를 가져옴
    override fun onLocationUpdated(location: Location?) {
        mapPoint = MapPoint.mapPointWithGeoCoord(location!!.latitude, location.longitude)
        mapView.setMapCenterPoint(mapPoint, true)
        mapView.setZoomLevel(5, true)
        val marker = MapPOIItem()
        marker.itemName = "집"
        marker.mapPoint = mapPoint
        marker.markerType = MapPOIItem.MarkerType.BluePin
        marker.selectedMarkerType = MapPOIItem.MarkerType.RedPin
        mapView.addPOIItem(marker)
        callKakaoKeyword("상담 센터", location.longitude.toString(), location.latitude.toString())
    }

    // 작성일자 22.05.11
    // 작성자 : 이병훈
    // 기능 : onLocationUpdated 에서 검색 결과에 마커를 찍어 보여줌
    private fun callKakaoKeyword(address:String, x:String,y:String){
        val kakao = MutableLiveData<KakaoData>()
        val marker = MapPOIItem()
        val location_list: RecyclerView = findViewById(R.id.location_list)
        kakaoApi.getKakaoAddress(KakaoApi.API_KEY, address = address, x=x,y=y)
            .enqueue(object : retrofit2.Callback<KakaoData>{
                override fun onResponse(call: Call<KakaoData>, response: Response<KakaoData>) {
                    kakao.value = response.body()
                    if(kakao.value == null){
                        Log.i("kakao", "없음")
                    }else {
                        location_list.adapter = LocationAdapter(this@NearMap, kakao.value!!.documents as ArrayList<Document>)
                        val lm = LinearLayoutManager(this@NearMap)
                        location_list.layoutManager = lm
                        for (i in kakao.value!!.documents.indices) {
                            mapPoint = MapPoint.mapPointWithGeoCoord(kakao.value!!.documents[i].y.toDouble(), kakao.value!!.documents[i].x.toDouble())
                            marker.itemName = kakao.value!!.documents[i].place_name
                            marker.mapPoint = mapPoint
                            marker.markerType = MapPOIItem.MarkerType.BluePin
                            marker.selectedMarkerType = MapPOIItem.MarkerType.RedPin
                            mapView.addPOIItem(marker)
                        }
                    }
                }
                override fun onFailure(call: Call<KakaoData>, t:Throwable){
                    t.printStackTrace()
                }
            })
    }

    // 작성일자 22.05.11
    // 작성자 : 이병훈
    // 기능 : BottomSheet 에 검색 결과를 리스트 형식으로 출력하는 adapter
    inner class LocationAdapter(val context: Context, private val arrayList: ArrayList<Document>) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val view: View = LayoutInflater.from(context).inflate(R.layout.item_map, parent, false)
            return Holder(view)
        }
        override fun getItemCount(): Int {
            return arrayList.size
        }
        @SuppressLint("SimpleDateFormat")
        private lateinit var mapPoint :MapPoint
        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
            (viewHolder as Holder).item_no.text = (i+1).toString()
            (viewHolder as Holder).location_name.text = arrayList[i].place_name
            (viewHolder as Holder).location_distance.text = (arrayList[i].distance.toInt()*0.001).toString().substring(0,3) + " Km"
            viewHolder.itemView.setOnClickListener {
                mapPoint = MapPoint.mapPointWithGeoCoord(arrayList[i].y.toDouble(),arrayList[i].x.toDouble())
                mapView.setMapCenterPoint(mapPoint, true)
            }
        }

        inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val item_no: TextView = itemView.findViewById(R.id.item_no)
            val location_name: TextView = itemView.findViewById(R.id.location_name)
            val location_distance: TextView = itemView.findViewById(R.id.location_distance)
        }

    }
}
