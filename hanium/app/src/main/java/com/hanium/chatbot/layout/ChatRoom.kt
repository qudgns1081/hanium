package com.hanium.chatbot.layout

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.hanium.chatbot.*
import com.hanium.chatbot.adapter.ChatAdapter
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URISyntaxException
import java.util.*
import kotlin.collections.ArrayList
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout


class ChatRoom:AppCompatActivity() {
    private var arrayList = arrayListOf<ChatData>()
    private val mAdapter = ChatAdapter(this, arrayList)

    private var hasConnection: Boolean = false
    private val mSocket: Socket = IO.socket("http://34.206.243.43:3000/")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chatroom)

        val chatSend:Button = findViewById(R.id.chat_send)
        val getContentB:Button = findViewById(R.id.getContent)
        val bottomB:Button = findViewById(R.id.bottomB)
        val chatList:RecyclerView = findViewById(R.id.chat_list)
        val root:RelativeLayout = findViewById(R.id.root)
        var position = 0
        var getContentCount = 0
        val sharedPreference = getSharedPreferences("other", 0)
        val im = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        val url = "http://34.206.243.43:3000/"
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val getContent = retrofit.create(GetContent::class.java)

        firstChat()

        //소프트 키보드가 올라오면 chatList 도 따라 올라감
        root.viewTreeObserver.addOnGlobalLayoutListener {
            if(im.isAcceptingText && position ==1)
            {
                chatList.scrollToPosition(arrayList.size - 1)
            }
        }

        chatList.adapter = mAdapter
        val lm = LinearLayoutManager(this)
        chatList.layoutManager = lm
        chatList.setHasFixedSize(true)
        // chatList position 에 따라 버튼을 보여주거나 숨김
        chatList.setOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

                    if (!chatList.canScrollVertically(-1)) {
                        position = 0
                        bottomB.visibility = View.VISIBLE
                    } else if (!chatList.canScrollVertically(1)) {
                        position = 1
                        bottomB.visibility = View.GONE
                    } else {
                        position = 0
                        bottomB.visibility = View.VISIBLE
                    }
            }
        })


        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : 이전 대화내용 가져오기
        getContentB.setOnClickListener {
            if(getContentCount == 0) {
                val systemChat = ChatData("이전 대화입니다", "system", System.currentTimeMillis())
                arrayList.add(0, systemChat)
                getContentCount++
            }
            //retrofit2 를 이용한 데이터 전송 후 응답 받기
            if (arrayList.size != 0) {
                getContent.postRequest(sharedPreference.getString("id", "").toString(), arrayList[arrayList.size - 1].date).enqueue((object :
                    Callback<ArrayList<ChatData>> {
                    override fun onFailure(call: Call<ArrayList<ChatData>>, t: Throwable) {
                    }

                    @SuppressLint("NotifyDataSetChanged")
                    override fun onResponse(call: Call<ArrayList<ChatData>>, response: Response<ArrayList<ChatData>>)
                    {
                        for (i in 0 until response.body()!!.size) {
                            arrayList.add(0, response.body()!![i])
                        }
                        mAdapter.notifyDataSetChanged()
                        chatList.scrollToPosition(arrayList.size - 1)
                    }
                }))
            }
        }

        bottomB.setOnClickListener {
            chatList.smoothScrollToPosition(arrayList.size - 1)
        }

        chatSend.setOnClickListener {
            sendMessage()
        }

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : socket.io 연결
        if (savedInstanceState != null) {
            hasConnection = savedInstanceState.getBoolean("hasConnection")
        }

        if (hasConnection) {

        } else {
            //소켓연결
            try {
                mSocket.connect()
                Log.d("Connected", "OK")
            } catch (e: URISyntaxException) {
                Log.d("ERR", e.toString())
            }

            //서버에 신호 보내는거같음 밑에 에밋 리스너들 실행
            //socket.on은 수신
            mSocket.on("connect user", onNewUser)
            mSocket.on("chat message", onNewMessage)

            val userId = JSONObject()
            try {
                userId.put("username", sharedPreference.getString("id", "") + " Connected")
                userId.put("roomName", sharedPreference.getString("id", "").toString())
                Log.e("username",sharedPreference.getString("id", "") + " Connected")

                mSocket.emit("connect user", userId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

        hasConnection = true
    }

    // 작성일자 22.05.10
    // 작성자 : 이병훈
    // 기능 : 첫 대화 내용 입력
    private fun firstChat(){
        val firstChat = ChatData("무엇을 도와드릴까요", "bot", System.currentTimeMillis())
        arrayList.add(firstChat)
    }

    // 작성일자 22.05.09
    // 작성자 : 이병훈
    // 기능 : socket.io 메세지 전송
    @SuppressLint("NotifyDataSetChanged", "SimpleDateFormat")
    private fun sendMessage() {
        val sharedPreference = getSharedPreferences("other", 0)
        val chatContent:EditText = findViewById(R.id.chat_content)
        val now = System.currentTimeMillis()
        val message = chatContent.text.toString().trim { it <= ' ' }
        if (TextUtils.isEmpty(message)) {
            return
        }
        chatContent.setText("")
        val jsonObject = JSONObject()
        try {
            jsonObject.put("id", sharedPreference.getString("id", "").toString())
            jsonObject.put("content", message)
            jsonObject.put("date_time", now)
            jsonObject.put("roomName", sharedPreference.getString("id", "").toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        Log.e("챗룸", "sendMessage: " + mSocket.emit("chat message", jsonObject))
        Log.e("sendmmm",sharedPreference.getString("id", "").toString() )
    }

    // 작성일자 22.05.09
    // 작성자 : 이병훈
    // 기능 : socket.io 메세지 응답
    @SuppressLint("NotifyDataSetChanged")
    internal var onNewMessage: Emitter.Listener = Emitter.Listener { args ->
        val sharedPreference = getSharedPreferences("other", 0)
        runOnUiThread(Runnable {
            val data = args[0] as JSONObject
            val name: String
            val content: String
            val date: Long
            try {
                name = sharedPreference.getString("id","").toString()
                content = data["content"].toString()
                date = data["date_time"].toString().toLong()

                val format = ChatData(content, name, date)
                arrayList.add(format)
                mAdapter.notifyDataSetChanged()
            } catch (e: Exception) {
                return@Runnable
            }
        })
    }

    // 작성일자 22.05.09
    // 작성자 : 이병훈
    // 기능 : socket.io connect user, on new user 명령 실행
    internal var onNewUser: Emitter.Listener = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            val length = args.size

            if (length == 0) {
                return@Runnable
            }
            var username = args[0].toString()
            try {
                val `object` = JSONObject(username)
                username = `object`.getString("username")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        })
    }

}