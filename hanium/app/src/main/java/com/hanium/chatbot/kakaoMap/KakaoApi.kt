package com.hanium.chatbot.kakaoMap

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

class KakaoApi {
    companion object {
        const val BASE_URL = "https://dapi.kakao.com/"
        const val API_KEY = "KakaoAK 3fc3cc4b43eed409bca0073e8863a680"
    }
}
    interface KakaoApiService {
        @GET("v2/local/search/keyword.json")
        fun getKakaoAddress(
            @Header("Authorization") key:String,
            @Query("query") address: String,
            @Query("x") x:String,
            @Query("y") y:String
            ): Call<KakaoData>

    }