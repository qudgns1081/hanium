package com.hanium.chatbot.layout

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.hanium.chatbot.R
import java.util.concurrent.Executor

private lateinit var executor: Executor
private lateinit var biometricPrompt: BiometricPrompt
private lateinit var promptInfo: BiometricPrompt.PromptInfo
class User:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user)

        val userId:TextView = findViewById(R.id.user_id)
        val logout: Button = findViewById(R.id.logout)
        val sharedPreference = getSharedPreferences("other", 0)
        val editor = sharedPreference.edit()
        val resetPass : Button = findViewById(R.id.reset_pass)
        val resetP : Button = findViewById(R.id.resetP)
        val fingerPrint : Button = findViewById(R.id.fingerPrint)
        val id = sharedPreference.getString("id","")

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : 지문인식 클래스 정의
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int,
                                                   errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Toast.makeText(applicationContext, "지문정보가 일치하지 않습니다", Toast.LENGTH_SHORT).show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    editor.putString(id+"fingerprint","1").apply()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(applicationContext, "지문정보가 일치하지 않습니다", Toast.LENGTH_SHORT).show()
                }
            })

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : 지문인식 UI 메세지 설정
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("사용자 인증")
            .setSubtitle("지문인식을 사용하기 위해 인증")
            .setNegativeButtonText("취 소")
            .build()

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : 지문인식이 가능한 기종을 판단 후 메뉴 활성화 및 비활성화
        val biometricManager = BiometricManager.from(this)
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS ->
                fingerPrint.visibility = View.VISIBLE
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                fingerPrint.visibility = View.GONE
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                fingerPrint.visibility = View.GONE
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                fingerPrint.visibility = View.GONE
            }
            BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {
                TODO()
            }
            BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {
                TODO()
            }
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {
                TODO()
            }
        }


        // 작성일자 22.04.29
        // 작성자 : 이병훈
        // 기능 : 로그아웃
        logout.setOnClickListener {
            editor.putString("id","")
            editor.putString("pw","")
            editor.putString("login", "false")
            editor.apply()
            val intent = Intent(this, Loading::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : pass 초기화
        resetPass.setOnClickListener {
            val intent = Intent(this, Pass::class.java)
            intent.putExtra("type", "reset")
            startActivity(intent)
        }

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : pass 삭제
        resetP.setOnClickListener {
            editor.putString(id, null)
            editor.putString(id+"fingerprint","").apply()
            editor.apply()
        }

        // 작성일자 22.05.09
        // 작성자 : 이병훈
        // 기능 : 지문인식 활성화
        fingerPrint.setOnClickListener {
            if(sharedPreference.getString(id,"") == "" || sharedPreference.getString(id,"") == null){
                Toast.makeText(this, "지문인식을 사용하기 위해 PIN 번호를 생성해 주세요", Toast.LENGTH_SHORT).show()
            }else {
                biometricPrompt.authenticate(promptInfo)
            }
        }
    }
}