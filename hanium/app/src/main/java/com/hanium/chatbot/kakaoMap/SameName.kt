package com.hanium.chatbot.kakaoMap

data class SameName(
    val keyword: String,
    val region: List<Any>,
    val selected_region: String
)