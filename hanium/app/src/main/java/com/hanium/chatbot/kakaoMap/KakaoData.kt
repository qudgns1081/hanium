package com.hanium.chatbot.kakaoMap

data class KakaoData(
    val documents: List<Document>,
    val meta: Meta
)