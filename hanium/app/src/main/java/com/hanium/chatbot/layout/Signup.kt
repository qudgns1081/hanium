package com.hanium.chatbot.layout

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.hanium.chatbot.*
import com.hanium.chatbot.Signup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Signup:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup)

        val url = "http://34.206.243.43:3000/"
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val signupApi = retrofit.create(Signup::class.java)
        val idCheckApi = retrofit.create(IdCheck::class.java)
        val pwL : LinearLayout = findViewById(R.id.pwLayout)
        val idC : Button = findViewById(R.id.id_check)
        val id : EditText = findViewById(R.id.signup_id)
        val pw : EditText = findViewById(R.id.signup_pw)
        val pwC : EditText = findViewById(R.id.pw_check)
        val authCode : EditText = findViewById(R.id.authCode)
        var signupCount = 0
        var code = ""

        // 작성일자 22.04.29
        // 작성자 : 이병훈
        // 기능 : id 중복 체크 후 이메일 전송
        idC.setOnClickListener {
            when(signupCount){
                0 -> {
                    idCheckApi.postRequest(id.text.toString()).enqueue((object :
                        Callback<AuthData> {
                        override fun onFailure(call: Call<AuthData>, t: Throwable) {
                            Log.d("response : ", t.toString())
                        }
                        override fun onResponse(call: Call<AuthData>, response: Response<AuthData>) {
                            if (response.body()?.code.toString() == "false") {
                                Toast.makeText(this@Signup, "사용중인 아이디입니다.", Toast.LENGTH_SHORT).show()
                            }else{
                                id.visibility = View.GONE
                                authCode.visibility = View.VISIBLE
                                authCode.requestFocus()
                                code = response.body()?.code.toString()
                                signupCount++
                            }
                        }
                    }))
                }
                1 -> {
                    if(authCode.text.toString() == code){
                        authCode.visibility = View.GONE
                        pwL.visibility = View.VISIBLE
                        pw.requestFocus()
                        signupCount++
                    }
                }
                2 -> {
                    if(pw.text.toString() == pwC.text.toString()) {
                        signupApi.postRequest(id.text.toString(), pw.text.toString()).enqueue((object :
                            Callback<UserInfo> {
                            override fun onFailure(call: Call<UserInfo>, t: Throwable) {
                                Log.d("response : ", t.toString())

                            }

                            override fun onResponse(call: Call<UserInfo>, response: Response<UserInfo>) {
                                if(response.body()?.results.toString() == "true"){
                                    Toast.makeText(this@Signup, "회원가입 성공.", Toast.LENGTH_SHORT).show()
                                    finish()
                                }else {
                                    Toast.makeText(this@Signup, "회원가입에 실패하였습니다.", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }))
                    }else {
                        Toast.makeText(this, "비밀번호가 일치하지 않습니다", Toast.LENGTH_SHORT).show()
                    }
                    }
                }
            }
        }
}