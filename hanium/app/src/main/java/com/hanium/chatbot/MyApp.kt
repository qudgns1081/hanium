package com.hanium.chatbot

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.*
import com.hanium.chatbot.layout.MainActivity
import com.hanium.chatbot.layout.Pass

class MyApp : Application(), LifecycleEventObserver {

    companion object{
        var isForeground =false
    }
    private var count = 0
    private val lifecycleEventObserver = LifecycleEventObserver { source, event ->
        val sharedPreference = getSharedPreferences("other", 0)
        val id = sharedPreference.getString("id","")
        if (event == Lifecycle.Event.ON_CREATE ) {
            if (sharedPreference.getString("login", "") == "true" && (sharedPreference.getString(id, "").toString() == "" || sharedPreference.getString(id, "") == null)
            ) {
                Log.i("tag", "1")
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
          //  if (event == Lifecycle.Event.ON_RESUME ) { }
        if ( event == Lifecycle.Event.ON_RESUME ) {
            val id = sharedPreference.getString("id","")
                if (sharedPreference.getString("login","") == "true" && (sharedPreference.getString(id,"").toString() != "" || sharedPreference.getString(id,null) != null)) {
                    val intent = Intent(this, Pass::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    if(count == 0){
                        intent.putExtra("type", "first")
                        startActivity(intent)
                        count=1
                    }else {
                        startActivity(intent)
                    }
                }
        }
    }

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(lifecycleEventObserver)
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        TODO("Not yet implemented")
    }
}