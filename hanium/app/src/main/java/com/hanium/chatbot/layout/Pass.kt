package com.hanium.chatbot.layout

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.hanium.chatbot.R
import java.util.concurrent.Executor
import kotlin.collections.ArrayList
import kotlin.system.exitProcess

private lateinit var executor: Executor
private lateinit var biometricPrompt: BiometricPrompt
private lateinit var promptInfo: BiometricPrompt.PromptInfo
class Pass:AppCompatActivity() {
    private var passCount = 0
    private val passArray = ArrayList<Int>()
    private var resetPass1 = ""
    private var type =""
    private var resetCount = 0
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pass)

        val pass1: Button = findViewById(R.id.pass_1)
        val pass2: Button = findViewById(R.id.pass_2)
        val pass3: Button = findViewById(R.id.pass_3)
        val pass4: Button = findViewById(R.id.pass_4)
        val pass5: Button = findViewById(R.id.pass_5)
        val pass6: Button = findViewById(R.id.pass_6)
        val pass7: Button = findViewById(R.id.pass_7)
        val pass8: Button = findViewById(R.id.pass_8)
        val pass9: Button = findViewById(R.id.pass_9)
        val pass0: Button = findViewById(R.id.pass_0)
        val passX: ImageButton = findViewById(R.id.pass_x)
        val error:TextView = findViewById(R.id.error)

        //User class 에서 넘어온 값 받기
        type = intent.getStringExtra("type").toString()
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val vibrationEffect = VibrationEffect.createOneShot(100, 30)

        executor = ContextCompat.getMainExecutor(this)
        val sharedPreference = getSharedPreferences("other", 0)
        val id = sharedPreference.getString("id","")

       biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    when (type) {
                        "first" -> {
                            startActivity(Intent(this@Pass,MainActivity::class.java))
                            finish()
                        }
                        "reset" -> {
                            resetCount = 1
                            error.text = "새로운 PIN 번호를 입력해 주세요."
                        }
                        else -> {
                            finish()
                        }
                    }
                }
                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    vibrator.vibrate(vibrationEffect)

                }
            })
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("사용자 인증")
            .setNegativeButtonText("취 소")
            .build()


        when (type) {
            "first" -> {
                if(sharedPreference.getString(id+"fingerprint","").toString() == "1") {
                    biometricPrompt.authenticate(promptInfo)
                }
            }
            "reset" -> {
                error.text = "현재 PIN 번호를 입력해 주세요"
                if(sharedPreference.getString(id+"fingerprint","").toString() == "1") {
                    biometricPrompt.authenticate(promptInfo)
                }
            }
            else -> {
                if(sharedPreference.getString(id+"fingerprint","").toString() == "1") {
                    biometricPrompt.authenticate(promptInfo)
                }
            }
        }
        if(sharedPreference.getString(id, "").toString() == "" || sharedPreference.getString(id, "") == null) {
            resetCount = 1
            error.text = "새로운 PIN 번호를 입력해 주세요."
        }

        // 작성일자 22.04.27
        // 작성자 : 이병훈
        // 기능 : 버튼 클릭 시 각 내용 실행
        val clickListener: View.OnClickListener = View.OnClickListener { view ->
            when (view.id) {
                R.id.pass_1 -> {
                    passArray.add(1)
                    numInput() }
                R.id.pass_2 -> {
                    passArray.add(2)
                    numInput() }
                R.id.pass_3 -> {
                    passArray.add(3)
                    numInput() }
                R.id.pass_4 -> {
                    passArray.add(4)
                    numInput() }
                R.id.pass_5 -> {
                    passArray.add(5)
                    numInput() }
                R.id.pass_6 -> {
                    passArray.add(6)
                    numInput() }
                R.id.pass_7 -> {
                    passArray.add(7)
                    numInput()}
                R.id.pass_8 -> {
                    passArray.add(8)
                    numInput() }
                R.id.pass_9 -> {
                    passArray.add(9)
                    numInput()}
                R.id.pass_0 -> {
                    passArray.add(0)
                    numInput()}
                R.id.pass_x -> {
                    val count = pwDel()
                    count.setImageResource(R.drawable.pass_before)
                    if(passArray.size != 0) {
                        passArray.removeAt(passCount-1)
                        passCount--
                    }
                }
            }
        }
        pass1.setOnClickListener(clickListener)
        pass2.setOnClickListener(clickListener)
        pass3.setOnClickListener(clickListener)
        pass4.setOnClickListener(clickListener)
        pass5.setOnClickListener(clickListener)
        pass6.setOnClickListener(clickListener)
        pass7.setOnClickListener(clickListener)
        pass8.setOnClickListener(clickListener)
        pass9.setOnClickListener(clickListener)
        pass0.setOnClickListener(clickListener)
        passX.setOnClickListener(clickListener)
    }

    // 작성일자 22.04.27
    // 작성자 : 이병훈
    // 기능 : 패스워드 하나 누를때마다 위치값 반환
    private fun pwAdd(): ImageView {
        val pw1: ImageView = findViewById(R.id.pw_1)
        val pw2: ImageView = findViewById(R.id.pw_2)
        val pw3: ImageView = findViewById(R.id.pw_3)
        val pw4: ImageView = findViewById(R.id.pw_4)
        return when (passCount) {
            0 -> pw1
            1 -> pw2
            2 -> pw3
            3 -> pw4
            else -> pw1
        }
    }
    // 작성일자 22.04.27
    // 작성자 : 이병훈
    // 기능 : 패스워드 하나 지울때마다 위치값 반환
    private fun pwDel(): ImageView {
        val pw1: ImageView = findViewById(R.id.pw_1)
        val pw2: ImageView = findViewById(R.id.pw_2)
        val pw3: ImageView = findViewById(R.id.pw_3)
        val pw4: ImageView = findViewById(R.id.pw_4)
        return when (passCount-1) {
            0 -> pw1
            1 -> pw2
            2 -> pw3
            3 -> pw4
            else -> pw1
        }
    }
    // 작성일자 22.04.28
    // 작성자 : 이병훈
    // 기능 : 패드워드 하나 누를때 이미지 변환, 4개의 값을 입력 하면 다음 함수 실행
    private fun numInput(){
        val pw1: ImageView = findViewById(R.id.pw_1)
        val pw2: ImageView = findViewById(R.id.pw_2)
        val pw3: ImageView = findViewById(R.id.pw_3)
        val pw4: ImageView = findViewById(R.id.pw_4)
        val count = pwAdd()
        count.setImageResource(R.drawable.pass_after)
        passCount++
        if (passCount == 4) {
            if(type == "reset") {
                resetPass()
            }else {
                passCheck()
            }
            passCount = 0
                passArray.clear()
                Handler(Looper.getMainLooper()).postDelayed({
                    pw1.setImageResource(R.drawable.pass_before)
                    pw2.setImageResource(R.drawable.pass_before)
                    pw3.setImageResource(R.drawable.pass_before)
                    pw4.setImageResource(R.drawable.pass_before)
                }, 300)
        }
    }
    // 작성일자 22.04.30
    // 작성자 : 이병훈
    // 기능 : 패스워드 리셋

    @SuppressLint("SetTextI18n")
    private fun resetPass() {
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val vibrationEffect = VibrationEffect.createOneShot(300, 50)
        val sharedPreference = getSharedPreferences("other", 0)
        val id = sharedPreference.getString("id","")
        val error:TextView = findViewById(R.id.error)
        val editor = sharedPreference.edit()
        if(resetCount == 0){
            Log.i("tag","reset0")
            val pass = passArray[0].toString()+passArray[1].toString()+passArray[2].toString()+passArray[3].toString()
            if(pass == sharedPreference.getString(id,"").toString()) {
                resetCount=1
                error.text = "새로운 PIN 번호를 입력해 주세요."
            }else {
                vibrator.vibrate(vibrationEffect)
                error.text = "비밀번호가 일치하지 않습니다."
                Handler(Looper.getMainLooper()).postDelayed({
                    error.text = "현재 PIN 번호를 입력해 주세요."
                },300)
            }
        }else if(resetCount == 1){
            Log.i("tag","reset1")
            resetPass1 = passArray.toString()
            resetCount=2
            error.text = "PIN 번호를 다시 입력해 주세요."
        }else {
            if(resetPass1 == passArray.toString()){
                val pass = passArray[0].toString()+passArray[1].toString()+passArray[2].toString()+passArray[3].toString()
                resetCount = 0
                editor.putString(id, pass)
                editor.apply()
                finish()
            }else {
                vibrator.vibrate(vibrationEffect)
                error.text = "PIN 번호가 일치하지 않습니다."
                Handler(Looper.getMainLooper()).postDelayed({
                    error.text = "PIN 번호를 다시 입력해 주세요."
                },300)
            }
        }
    }
    // 작성일자 22.04.28
    // 작성자 : 이병훈
    // 기능 : pass 값이 저장되어있는 값이랑 같은지 확인
    private fun passCheck() {
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val vibrationEffect = VibrationEffect.createOneShot(300, 50)
        val error:TextView = findViewById(R.id.error)
        val sharedPreference = getSharedPreferences("other", 0)
        val id = sharedPreference.getString("id","")
        val pass = passArray[0].toString()+passArray[1].toString()+passArray[2].toString()+passArray[3].toString()
        if(pass == sharedPreference.getString(id,"")){
            if(type == "first") {
                startActivity(Intent(this,MainActivity::class.java))
            }
            finish()
        }else {
            vibrator.vibrate(vibrationEffect)
            error.text = "PIN 번호가 맞지 않습니다."
            Handler(Looper.getMainLooper()).postDelayed({
                error.text = ""
            },300)
        }
    }

    private var mBackWait:Long = 0
    override fun onBackPressed() {
        if (type == "reset") {
            finish()
        }
         else{   if (System.currentTimeMillis() - mBackWait >= 2000) {
                mBackWait = System.currentTimeMillis()
                Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show()
            } else {
                ActivityCompat.finishAffinity(this)
                System.runFinalization()
                exitProcess(0) //액티비티 종료
            }
        }
    }
}