package com.hanium.chatbot.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hanium.chatbot.ChatData
import com.hanium.chatbot.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatAdapter(val context: Context, private val arrayList: ArrayList<ChatData>) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //getItemViewType 에서 뷰타입 1을 리턴받았다면 내채팅레이아웃을 받은 Holder를 리턴
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_my_chat, parent, false)
            return Holder(view)

    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        lateinit var preferences: SharedPreferences
        val date = Date(arrayList[i].date!!)
        val sdf = SimpleDateFormat("hh:mm")
        val getTime = sdf.format(date)
        when {
            arrayList[i].no.toString() == "bot" -> {
                (viewHolder as Holder).answerLayout.visibility = View.VISIBLE
                (viewHolder as Holder).myChatLayout.visibility = View.GONE
                (viewHolder as Holder).systemLayout.visibility = View.GONE
                (viewHolder as Holder).answer.text = arrayList[i].content
                (viewHolder as Holder).answerChatTime.text = getTime

            }
            arrayList[i].no.toString() == "system" -> {
                (viewHolder as Holder).answerLayout.visibility = View.GONE
                (viewHolder as Holder).myChatLayout.visibility = View.GONE
                (viewHolder as Holder).systemLayout.visibility = View.VISIBLE
                (viewHolder as Holder).system.text = arrayList[i].content
            }
            else -> {
                (viewHolder as Holder).myChatLayout.visibility = View.VISIBLE
                (viewHolder as Holder).answerLayout.visibility = View.GONE
                (viewHolder as Holder).systemLayout.visibility = View.GONE
                (viewHolder as Holder).myChat.text = arrayList[i].content
                (viewHolder as Holder).myChatTime.text = getTime
            }
        }

    }

    //내가친 채팅 뷰홀더
    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //친구목록 모델의 변수들 정의하는부분
        val myChatLayout:LinearLayout = itemView.findViewById(R.id.myChat)
        val answerLayout:LinearLayout = itemView.findViewById(R.id.answerChat)
        val systemLayout:LinearLayout = itemView.findViewById(R.id.systemChat)
        val myChat:TextView = itemView.findViewById(R.id.myText)
        val system:TextView = itemView.findViewById(R.id.system)
        val myChatTime:TextView = itemView.findViewById(R.id.date2)
        val answer:TextView = itemView.findViewById(R.id.answer)
        val answerChatTime:TextView = itemView.findViewById(R.id.date)
    }

}