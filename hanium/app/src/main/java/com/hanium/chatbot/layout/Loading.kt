package com.hanium.chatbot.layout

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.hanium.chatbot.Login
import com.hanium.chatbot.R
import com.hanium.chatbot.UserInfo
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

import android.view.animation.Animation
import android.view.animation.AnimationUtils


class Loading: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loading)

        val url = "http://34.206.243.43:3000/"

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val loginApi = retrofit.create(Login::class.java)
        val login : Button = findViewById(R.id.login)
        val signup : Button = findViewById(R.id.signup)
        val id : EditText = findViewById(R.id.id)
        val pw : EditText = findViewById(R.id.pw)
        val loginBox : LinearLayout = findViewById(R.id.loginBox)
        val sharedPreference = getSharedPreferences("other", 0)
        val editor = sharedPreference.edit()

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : 로그인 화면 애니메이션
        Handler(Looper.getMainLooper()).postDelayed({
            if(sharedPreference.getString("id", "") == "" || sharedPreference.getString("id", "") == null) {
                loginBox.visibility = View.VISIBLE
                val animation: Animation = AnimationUtils.loadAnimation(this, R.anim.slide_up)
                loginBox.animation = animation
            }else {
                // 작성일자 22.04.28
                // 작성자 : 이병훈
                // 기능 : 자동 로그인
                loginApi.postRequest(sharedPreference.getString("id","").toString(), sharedPreference.getString("pw","").toString()).enqueue((object: Callback<UserInfo> {
                    override fun onFailure(call: Call<UserInfo>, t: Throwable) {
                        Log.d("response : ", t.toString())
                        Toast.makeText(this@Loading, "로그인 할 수 없습니다. 다시 시도해 주세요", Toast.LENGTH_SHORT).show()
                        loginBox.visibility = View.VISIBLE
                        val animation: Animation = AnimationUtils.loadAnimation(this@Loading, R.anim.slide_up)
                        loginBox.animation = animation
                    }
                    override fun onResponse(call: Call<UserInfo>, response: Response<UserInfo>) {
                        if(response.body()?.results.toString() == "true") {
                        }else {
                            Toast.makeText(this@Loading, "로그인 할 수 없습니다. 다시 시도해 주세요", Toast.LENGTH_SHORT).show()
                            loginBox.visibility = View.VISIBLE
                            val animation: Animation = AnimationUtils.loadAnimation(this@Loading, R.anim.slide_up)
                            loginBox.animation = animation
                        }
                    }
                }))
            }
        },1500)

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : 로그인
        login.setOnClickListener {
            loginApi.postRequest(id.text.toString(), pw.text.toString()).enqueue((object: Callback<UserInfo> {
                    override fun onFailure(call: Call<UserInfo>, t: Throwable) {
                        Log.d("response : ", t.toString())

                    }
                    override fun onResponse(call: Call<UserInfo>, response: Response<UserInfo>) {
                       if(response.body()?.results.toString() == "true") {
                           startActivity(Intent(this@Loading, MainActivity::class.java))
                           editor.putString("id", id.text.toString())
                           editor.putString("pw", pw.text.toString())
                           //테스트 pass 번호
                           editor.putString("login", "true")
                           editor.apply()
                           finish()
                       }
                       else
                           Toast.makeText(this@Loading, response.body()?.msg.toString(), Toast.LENGTH_SHORT).show()
                    }
                }))
            }

        signup.setOnClickListener {
            startActivity(Intent(this,Signup::class.java))
        }
    }

}