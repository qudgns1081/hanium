package com.hanium.chatbot.layout

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.hanium.chatbot.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val url = "http://34.206.243.43:3000/"

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val contentGet = retrofit.create(ContentGet::class.java)
        val contentInput = retrofit.create(ContentInput::class.java)

        val submit : Button = findViewById(R.id.submit)
        val contentI : EditText = findViewById(R.id.content_input)
        val contentText1 : TextView = findViewById(R.id.content_text1)
        val contentText2 : TextView = findViewById(R.id.content_text2)
        val contentText3 : TextView = findViewById(R.id.content_text3)
        val sharedPreference = getSharedPreferences("other", 0)
        val user : Button = findViewById(R.id.user)
        val map : Button = findViewById(R.id.map)
        val chat : Button = findViewById(R.id.chatroom)

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : 응원의 말 남기기 내용 가져오기
        contentGet.postRequest().enqueue((object:
            Callback<ContentData> {
            override fun onFailure(call: Call<ContentData>, t: Throwable) {
                Log.d("response : ", t.toString())

            }
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<ContentData>, response: Response<ContentData>) {
                contentText1.text = "내용 : " + response.body()?.content
                contentText2.text = "작성자 : " + response.body()?.id
            }
        }))

        // 작성일자 22.04.28
        // 작성자 : 이병훈
        // 기능 : 응원의 말 남기기 내용 등록하기
        submit.setOnClickListener {
            contentInput.postRequest(sharedPreference.getString("id", "").toString(), contentI.text.toString()).enqueue((object:
                Callback<ContentData> {
                override fun onFailure(call: Call<ContentData>, t: Throwable) {
                    Log.d("response : ", t.toString())
                }
                override fun onResponse(call: Call<ContentData>, response: Response<ContentData>) {
                    Log.d("tag",response.body().toString())
                }
            }))
        }
        user.setOnClickListener {
            startActivity(Intent(this, User::class.java))
        }
        map.setOnClickListener {
            startActivity(Intent(this, NearMap::class.java))
        }
        chat.setOnClickListener {
            startActivity(Intent(this, ChatRoom::class.java))
        }
    }

    // 작성일자 22.04.28
    // 작성자 : 이병훈
    // 기능 : 뒤로가기 2번 시 종료
    private var mBackWait:Long = 0
    override fun onBackPressed() {
        if(System.currentTimeMillis() - mBackWait >=2000 ) {
            mBackWait = System.currentTimeMillis()
            Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show()
        } else {
            ActivityCompat.finishAffinity(this)
            System.runFinalization()
            exitProcess(0) //액티비티 종료
        }
    }
}