package com.hanium.chatbot.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hanium.chatbot.R
import com.hanium.chatbot.kakaoMap.Document
import net.daum.mf.map.api.MapPoint
import net.daum.mf.map.api.MapView
import kotlin.collections.ArrayList

class LocationAdapter(val context: Context, private val arrayList: ArrayList<Document>) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_map, parent, false)
            return Holder(view)

    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    @SuppressLint("SimpleDateFormat")
    private lateinit var mapPoint :MapPoint
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as Holder).item_no.text = (i+1).toString()
        (viewHolder as Holder).location_name.text = arrayList[i].address_name
        mapPoint = MapPoint.mapPointWithGeoCoord(arrayList[i].y.toDouble(),arrayList[i].x.toDouble())
        viewHolder.itemView.setOnClickListener {
            MapView(context).setMapCenterPoint(mapPoint, true)

        }
    }

    //내가친 채팅 뷰홀더
    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item_no:TextView = itemView.findViewById(R.id.item_no)
        val location_name:TextView = itemView.findViewById(R.id.location_name)
    }

}